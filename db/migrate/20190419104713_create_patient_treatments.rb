class CreatePatientTreatments < ActiveRecord::Migration[5.1]
  def change
    create_table :patient_treatments do |t|
      t.belongs_to :patient, index: true
      t.belongs_to :treatment, index: true

      t.timestamps
    end
  end
end
