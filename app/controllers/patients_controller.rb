class PatientsController < ApplicationController
  before_action :set_patient, only: [:show, :edit, :update, :destroy]

  layout "normal", only: [:list, :show]

  # GET /patients
  # GET /patients.json
  def index
    @patients = Patient.all.order(created_at: :desc)
  end

  def list
    @patients = Patient.all.order(created_at: :desc)
  end

  # GET /patients/1
  # GET /patients/1.json
  def show
    @facility = Facility.first
  end

  # GET /patients/new
  def new
    @patient = Patient.new
  end

  # GET /patients/1/edit
  def edit
  end

  # POST /patients
  # POST /patients.json
  def create
    @patient = Patient.new(patient_params)

    respond_to do |format|
      if @patient.save
        set_associations
        format.html { redirect_to patients_url, notice: 'Patient was successfully created.' }
        format.json { render :show, status: :created, location: @patient }
      else
        format.html { render :new }
        format.json { render json: @patient.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /patients/1
  # PATCH/PUT /patients/1.json
  def update
    respond_to do |format|
      if @patient.update(patient_params)
        set_associations
        format.html { redirect_to patients_url, notice: 'Patient was successfully updated.' }
        format.json { render :show, status: :ok, location: @patient }
      else
        format.html { render :edit }
        format.json { render json: @patient.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /patients/1
  # DELETE /patients/1.json
  def destroy
    @patient.destroy
    respond_to do |format|
      format.html { redirect_to patients_url, notice: 'Patient was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  protected

  def set_associations
    @patient.allergies = Allergy.where(id: params[:allergy_ids])
    @patient.medication_orders = MedicationOrder.where(id: params[:medication_order_ids])
    @patient.diagnostic_procedures = DiagnosticProcedure.where(id: params[:diagnostic_procedure_ids])
    @patient.treatments = Treatment.where(id: params[:treatment_ids])
    @patient.diagnoses = Diagnosis.where(id: params[:diagnosis_ids])
    @patient.chronic_conditions = ChronicCondition.where(id: params[:chronic_condition_ids])
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_patient
      @patient = Patient.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def patient_params
      params.require(:patient).permit(:first_name, :middle_name, :last_name, :mr, :dob, :gender, :admission_id)
    end
end
