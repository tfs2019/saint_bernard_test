class OrderFrequency < ApplicationRecord
  has_many :medication_orders, foreign_key: :frequency_id

  enum unit: {Hour: 0}
end
