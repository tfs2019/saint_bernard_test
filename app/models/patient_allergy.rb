class PatientAllergy < ApplicationRecord
  belongs_to :patient, optional: true
  belongs_to :allergy, optional: true
end
