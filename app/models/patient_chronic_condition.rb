class PatientChronicCondition < ApplicationRecord
  belongs_to :patient, optional: true
  belongs_to :chronic_condition, optional: true
end
