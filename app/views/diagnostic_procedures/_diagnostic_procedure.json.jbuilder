json.extract! diagnostic_procedure, :id, :patient_id, :description, :moment, :created_at, :updated_at
json.url diagnostic_procedure_url(diagnostic_procedure, format: :json)
